import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PruebasPhantomjsIT {
    private static WebDriver driver = null;

    @Test
    public void tituloIndexTest()
    {
        DesiredCapabilities caps = new DesiredCapabilities(); 
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(),
        "El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }

    @Test
    public void reinicializarVotosTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.id("Carroll")).click();
        driver.findElement(By.name("B1")).submit();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.linkText("Reinicializar votos")).click();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.linkText("Ver votos")).click();
        int numOfRow = driver.findElements(By.xpath("//table/tbody/tr")).size();
        for(int iRow=2; iRow<= numOfRow; iRow++) {
            String nVotos = driver.findElement(By.xpath("//table/tbody/tr["+iRow+"]/td[2]")).getText();
            assertEquals("0", nVotos, "los votos no son todos a cero");
            System.out.println(nVotos);
        }
        driver.close();
        driver.quit();
    }

    @Test
    public void otroJugadorTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.name("txtOtros")).sendKeys("Saad");
        driver.findElement(By.id("Otro")).click();
        driver.findElement(By.name("B1")).submit();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.linkText("Ver votos")).click();
        int numOfRow = driver.findElements(By.xpath("//table/tbody/tr")).size();
        String nombre = driver.findElement(By.xpath("//table/tbody/tr["+numOfRow+"]/td[1]")).getText();
        String nVotos = driver.findElement(By.xpath("//table/tbody/tr["+numOfRow+"]/td[2]")).getText();
        assertEquals("Saad", nombre, "Este jugador no es Saad");
        assertEquals("1", nVotos, "Este jugador no tiene 1 voto");
        System.out.println(nombre+" tiene "+nVotos+" voto");
        driver.close();
        driver.quit();
    }

}