import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.swing.*;

public class Acb extends HttpServlet {

    private ModeloDatos bd;

    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        String nombreP = (String) req.getParameter("txtNombre");
        String nombre = (String) req.getParameter("R1");
        String accion = req.getParameter("accion");

        if (accion != null) {
            switch (accion) {
                case "reinicializar":
                    bd.reinicializarVotos();
                    res.sendRedirect(res.encodeRedirectURL("ReinicializarVotos.jsp"));
                    break;
                case "verVotos":
                    s.setAttribute("jugadores", bd.getAllJugadores());
                    res.sendRedirect(res.encodeRedirectURL("verVotos.jsp"));
                    break;
                default:
                    break;
            }
        } else {
            if (nombre.equals("Otros")) {
                nombre = (String) req.getParameter("txtOtros");
            }
            if (bd.existeJugador(nombre)) {
                bd.actualizarJugador(nombre);
            } else {
                bd.insertarJugador(nombre);
            }
            s.setAttribute("nombreCliente", nombreP);
            // Llamada a la página jsp que nos da las gracias
            res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
        }
    }

    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }

}
